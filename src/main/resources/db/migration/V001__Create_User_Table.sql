create table user
(
    id          LONG IDENTITY NOT NULL PRIMARY KEY,
    first_name  varchar(255)  not null,
    middle_name varchar(255),
    last_name   varchar(255)  not null,
    email       varchar(255)  not null,
    age         int           not null
);
create table user_strong
(
    id          LONG IDENTITY NOT NULL PRIMARY KEY,
    first_name  varchar(255)  not null,
    middle_name varchar(255),
    last_name   varchar(255)  not null,
    email       varchar(255)  not null,
    age         int           not null
);
