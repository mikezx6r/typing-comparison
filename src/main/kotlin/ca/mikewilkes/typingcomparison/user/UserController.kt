package ca.mikewilkes.typingcomparison.user

import org.springframework.stereotype.Service
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Validated
data class UserCreateDTO(
    @field:NotEmpty
    val firstName: String,
    val middleName: String?,
    @field:NotEmpty
    val lastName: String,
    @field:NotEmpty
    @field:Email
    val email: String,
    @field:NotNull
    @field:Min(0)
    @field:Max(200)
    val age: Int
)

@RestController
@RequestMapping("/user")
class UserController(val userService: UserService) {

    @GetMapping
    fun getUser(@RequestParam id: Long): UserInfoDTO? =
        userService.findUserById(id)

    @PostMapping
    fun addUser(@Valid @RequestBody user: UserCreateDTO): UserInfoDTO =
        userService.createUser(user)
}

data class UserInfoDTO(
    val id: Long,
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String,
    val age: Int
)

@Service
class UserService(val userRepository: UserRepository) {

    fun findUserById(id: Long): UserInfoDTO? {
        return userRepository.findById(id).orElse(null)?.toDTO()
    }

    fun createUser(userCreate: UserCreateDTO): UserInfoDTO {
        println("CreateUser in: $userCreate")

        require(userRepository.findFirstByEmail(userCreate.lastName) == null) { "User email already exists: $userCreate.email" }

        val newUser = userRepository.save(
            UserDB(
                null,
                userCreate.firstName,
                userCreate.middleName,
                userCreate.lastName,
                userCreate.email,
                userCreate.age
            )
        )
        println("DB object is: $newUser")
        return newUser.toDTO()
    }

    fun UserDB.toDTO(): UserInfoDTO = UserInfoDTO(
        id!!,
        email,
        middleName,
        lastName,
        firstName,
        age
    )
}