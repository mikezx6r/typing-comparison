package ca.mikewilkes.typingcomparison.user

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.PagingAndSortingRepository

interface UserRepository : PagingAndSortingRepository<UserDB, Long> {
    fun findFirstByEmail(email:String): UserDB?
}

@Table("USER")
data class UserDB(
    @field:Id val id: Long?,
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String,
    val age: Int
)