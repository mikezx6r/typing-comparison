package ca.mikewilkes.typingcomparison

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@SpringBootApplication
class TypingComparisonApplication

fun main(args: Array<String>) {
    runApplication<TypingComparisonApplication>(*args)
}

@ControllerAdvice
class ErrorHandler {
    @ExceptionHandler
    fun validationErrors(e: IllegalArgumentException): ResponseEntity<String> =
        ResponseEntity.badRequest().body(e.message)
}