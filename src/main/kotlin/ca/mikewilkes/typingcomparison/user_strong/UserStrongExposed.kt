package ca.mikewilkes.typingcomparison.user_strong

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Repository
import javax.sql.DataSource

@Repository
class UserStrongExposed(val dataSource: DataSource) : InitializingBean {

    fun findById(id: Long): StrongUserDB? =
        transaction {
            StrongUserDB.findById(id)
        }

    fun newUser(createDTO: UserStrongCreateDTO): StrongUserDB =
        transaction {
            StrongUserDB.new {
                firstName = createDTO.firstName
                middleName = createDTO.middleName
                lastName = createDTO.lastName
                email = createDTO.email
                age = createDTO.age
            }
        }

    fun findFirstByEmail(email:Email):StrongUserDB? =
        transaction {
            StrongUserDB.find { StrongUsersDB.email eq email.value }.limit(1).firstOrNull()
        }

    override fun afterPropertiesSet() {
        Database.connect(dataSource)
    }
}

class StrongUserDB(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<StrongUserDB>(StrongUsersDB)

    var firstName: FirstName by StrongUsersDB.firstName.transform({ it.value }, { FirstName(it) })
    var middleName: MiddleName? by StrongUsersDB.middleName.transform({ it?.value }, { it?.let { MiddleName(it) } })
    var lastName: LastName by StrongUsersDB.lastName.transform({ it.value }, { LastName(it) })
    var email: Email by StrongUsersDB.email.transform({ it.value }, { Email(it) })
    var age: Age by StrongUsersDB.age.transform({ it.value }, { Age(it) })

    override fun toString(): String {
        return "StrongUserDB(firstName=$firstName, middleName=$middleName, lastName=$lastName, email=$email, age=$age)"
    }
}


object StrongUsersDB : LongIdTable(name = "user_strong") {
    val firstName = varchar("first_name", 255)
    val middleName = varchar("middle_name", 255).nullable()
    val lastName = varchar("last_name", 255)
    val email = varchar("email", 255)
    val age = integer("age")
}
