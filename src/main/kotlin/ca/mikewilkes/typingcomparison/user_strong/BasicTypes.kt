package ca.mikewilkes.typingcomparison.user_strong

import kotlinx.serialization.Serializable
import javax.validation.ValidationException

@Serializable
inline class FirstName(val value: String) {
    init {
        require(value.isNotBlank()) {"First Name must have a value."}
    }
}

@Serializable
inline class LastName(val value: String) {
    init {
        require(value.isNotBlank()) {"Last Name must have a value."}
    }
}

@Serializable
inline class MiddleName(val value: String) {
    init {
        require(value.isNotBlank()) {"Middle Name must have a value."}
    }
}

@Serializable
inline class Email(val value: String) {
    init {
        require(value.isNotBlank()) {"Email must have a value."}
        require(value.contains("@")) { "Email must contain '@'" }
        require(!value.contains("scotibank")) { "Check spelling of 'scotiabank' in email address" }
    }
}

@Serializable
inline class Age(val value: Int) {
    init {
        if (value < 0 || value > 200) {
            throw ValidationException("Age must be between 0, and 200, but got $value")
        }
    }
}
