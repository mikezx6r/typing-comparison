package ca.mikewilkes.typingcomparison.user_strong

import kotlinx.serialization.Serializable
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Serializable
data class UserStrongCreateDTO(
    val firstName: FirstName,
    val middleName: MiddleName? = null,
    val lastName: LastName,
    val email: Email,
    val age: Age
)

@RestController
@RequestMapping("/userstrong")
class UserStrongController(val strongService: UserStrongService) {

    @GetMapping
    fun getUser(@RequestParam id: Long): UserStrongInfoDTO? =
        strongService.findUserById(id)

    @PostMapping
    fun createUser(@RequestBody userCreate: UserStrongCreateDTO): UserStrongInfoDTO =
        strongService.createUser(userCreate)
}

@Serializable
data class UserStrongInfoDTO(
    val id: Long,
    val firstName: FirstName,
    val middleName: MiddleName?,
    val lastName: LastName,
    val email: Email,
    val age: Age
)

@Service
class UserStrongService(val userStrongExposed: UserStrongExposed) {
    fun findUserById(id: Long): UserStrongInfoDTO? {
        return userStrongExposed.findById(id)?.toDTO()
    }

    fun createUser(userCreate: UserStrongCreateDTO): UserStrongInfoDTO {
        println("CreateUser in: $userCreate")
        require(userStrongExposed.findFirstByEmail(userCreate.email) == null) { "User email already exists: ${userCreate.email}" }

        val newUser = userStrongExposed.newUser(userCreate)
        println("DB object is: $newUser")
        return newUser.toDTO()
    }

    fun StrongUserDB.toDTO(): UserStrongInfoDTO = UserStrongInfoDTO(
        id.value,
        firstName,
        middleName,
        lastName,
        email,
        age
    )
}
